import { createApp } from 'vue'
import './assets/base.css'
import router from './router.js'
import store from './stores/store.js'
import App from './App.vue'

const app = createApp(App)

app.use(router)
app.use(store)
app.mount('#app')
