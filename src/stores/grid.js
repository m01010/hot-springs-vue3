import { defineStore } from 'pinia'

const useGridStore = defineStore('grid', {
  state: () => ({
    grid: null,
    previousGrid: null,
    height: null,
    width: null,
    waterSpreadInProgress: false,
    finalSpringStateReached: false,
    error: null,
    errorTimer: null,
  }),
  getters: {
    isGridCreated: (state) => state.grid !== null,
  },
  actions: {
    createGrid({ height, width }) {
      this.height = height
      this.width = width
      const constructedGrid = []
      // height = number of rows
      for (let rowI = 0; rowI < height; rowI += 1) {
        const row = []
        // width = number of cells in every row
        for (let cellI = 0; cellI < width; cellI += 1) {
          row.push({ id: `${rowI}.${cellI}`, value: null })
        }
        constructedGrid.push(row)
      }
      this.grid = constructedGrid
    },
    changeState({ rowI, cellI, action }) {
      if (this.grid[rowI][cellI].value === action) {
        this.grid[rowI][cellI].value = null
        return
      }
      this.grid[rowI][cellI].value = action
    },
    spreadWaterFromCellInDirection({ rowI, cellI }, direction) {
      switch (direction) {
        case 'left':
          if (cellI === 0) {
            // can't go to the left, it's the first cell
            break
          }
          if (this.grid[rowI][cellI - 1].value === null) {
            this.grid[rowI][cellI - 1].value = 'water'
          }
          break
        case 'right':
          if (cellI === this.width - 1) {
            // can't go to the right, it's the last cell
            break
          }
          if (this.grid[rowI][cellI + 1].value === null) {
            this.grid[rowI][cellI + 1].value = 'water'
          }
          break
        case 'top':
          if (rowI === 0) {
            // can't go to the top, it's the first row
            break
          }
          if (this.grid[rowI - 1][cellI].value === null) {
            this.grid[rowI - 1][cellI].value = 'water'
          }
          break
        case 'bottom':
          if (rowI === this.height - 1) {
            // can't go to the bottom, it's the last row
            break
          }
          if (this.grid[rowI + 1][cellI].value === null) {
            this.grid[rowI + 1][cellI].value = 'water'
          }
          break
        default:
      }
    },
    spreadNext() {
      this.previousGrid = JSON.stringify(this.grid)
      const waterCells = []
      // gather all water cells coordinates
      this.grid.forEach((row, rowI) => {
        row.forEach((cell, cellI) => {
          if (cell.value === 'water') {
            waterCells.push({ rowI, cellI })
          }
        })
      })
      // spread water for all adjacent cells
      waterCells.forEach(({ rowI, cellI }) => {
        this.spreadWaterFromCellInDirection({ rowI, cellI }, 'left')
        this.spreadWaterFromCellInDirection({ rowI, cellI }, 'right')
        this.spreadWaterFromCellInDirection({ rowI, cellI }, 'top')
        this.spreadWaterFromCellInDirection({ rowI, cellI }, 'bottom')
      })
      // check if any changes were made and return the result
      return JSON.stringify(this.grid) === this.previousGrid
    },
    showError(error) {
      clearTimeout(this.errorTimer)
      this.error = error
      this.errorTimer = setTimeout(() => (this.error = null), 3 * 1000)
    },
  },
})

export { useGridStore }
