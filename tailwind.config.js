import defaultTheme from 'tailwindcss/defaultTheme'

export default {
  content: ['./index.html', './{src,public}/**/*.{js,vue,html,css}'],
  theme: {
    screens: {
      xs: '475px',
      ...defaultTheme.screens,
    },
    extend: {
      colors: {
        vblue: '#2c3e50',
      },
    },
  },
  plugins: [],
}
