# Hot Springs Vacation
|                                                                                 |                      |
| ------------------------------------------------------------------------------- | -------------------- |
| <img src="docs/assets/hot-spring.svg" width="100px" alt="icon of hot springs with rocks laying around" /> | Hot Springs Vacation |

---

[![license badge](https://img.shields.io/static/v1?label=license&message=GPL3&color=D0EFFC&style=for-the-badge&logo=gnu)](https://www.gnu.org/philosophy/open-source-misses-the-point.html)

License: GPLv3-only

Deployed:
|   | URL | Description |
| - |  -  |      -      |
| <img src="docs/assets/hot-spring.svg" height="30px" alt="icon of hot springs with rocks laying around" /> | <https://hot-springs-vacation-vue3.surge.sh/> | deployed through surge.sh |

## Description
`Hot Springs` is a Vue 3 web app that helps create hot springs for your next
vacation!

- Create a grid by providing custom `width` and `height` values
- Dig some of the cells to locate springs
- Arrange rocks to help contain the streams and provide a comfortable lounging
area

Once the arrangement is complete, unfreeze the time with the `Let the water run`
button.
Watch the hot springs get created as your vacation awaits!

| -> | -> | -> |
| -- | -- | -- |
| ![grid page screenshot 1](docs/assets/grid1.png) | ![grid page screenshot 2](docs/assets/grid2.png) | ![grid page screenshot 3](docs/assets/grid3.png) |

## App Flow
This is a Vue 3 app utilizing Composition API with the `setup` syntax, Pinia for
store management, and Vue-Router set up with HTML5 History Mode. TailwindCSS is
used as the primary way to style pages and components.

The `GridControl` component renders the grid and allows grid interaction by
clicking on the cells directly.

The water spreads as far as it can by taking all the available cells outwards in
4 directions, as long as the path isn't blocked by rocks.

The grid can be reset or new dimensions can be provided to start over.

## Development
Node 18+ and Pnpm are required to run this app; to handle Node and Pnpm
versioning automatically, [Volta](https://volta.sh/) is recommended.

To install the dependencies and start the development version of the app, run
the following commands:
```shell
pnpm install
pnpm dev
```

## Building / Deployment
To bundle the app, run the following command:
```shell
pnpm build
```

The bundled files will be available in the `dist/` directory.

## Attribution
- Hot spring icon made by [smalllikeart](https://flaticon.com/authors/smalllikeart)
- Icons for cells created from the official gameplay screenshot of [`Heroes® of Might & Magic® III - HD Edition`](https://store.steampowered.com/app/297000/Heroes_of_Might__Magic_III__HD_Edition/)  
[![Heroes of Might & Magic III gameplay screenshot](docs/assets/homm3-screenshot.jpg)](https://cdn.cloudflare.steamstatic.com/steam/apps/297000/ss_548367faf1cfa549c88585cb9b01f13b05b05ab7.1920x1080.jpg)
